provider "aws" {
  region     = "eu-central-1"
}

resource "aws_vpc" "main"{
    cidr_block  =    "10.0.0.0/16"
    tags = {
        Name = "main"
      }
}

resource "aws_subnet" "public" {
  vpc_id     = "${aws_vpc.main.id}"
  map_public_ip_on_launch = true
  cidr_block = "10.0.0.0/24"
  tags = {
    Name = "Public"
  }
}

resource "aws_subnet" "private" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.1.0/24"
  tags = {
    Name = "Private"
  }
}

resource "aws_route_table" "public" {
  vpc_id     = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main.id}"
  }
}

resource "aws_route_table" "private" {
  vpc_id    = "${aws_vpc.main.id}"
}

resource "aws_internet_gateway" "main" {
    vpc_id  = "${aws_vpc.main.id}"
}

resource "aws_route_table_association" "public" {
    subnet_id      = "${aws_subnet.public.id}"
    route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "private" {
    subnet_id      = "${aws_subnet.private.id}"
    route_table_id = "${aws_route_table.private.id}"
}

resource "aws_key_pair" "auth" {
	key_name   = "terraform"
	public_key = "${file("terraform.pub")}"
}

resource "aws_instance" "my-aws-instance" {
  ami           = "ami-0cc0a36f626a4fdf5"
  subnet_id     = "${aws_subnet.public.id}"
  instance_type = "t2.micro"
  key_name      = "${aws_key_pair.auth.id}"
  tags          = {
        Name = "MyInstance"
  }

  vpc_security_group_ids = ["${aws_security_group.web.id}"]
  
  connection {
	type = "ssh"
	
	host = "${self.public_ip}"
	user = "ubuntu"
	private_key = "${file("terraform.pem")}"
  }
  
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo apt-get -y install nginx",
      "sudo service nginx start",
    ]
  }
}

resource "aws_security_group" "web" {
  name        = "web-sg"
  description = "Allow incoming HTTP connections."
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
      from_port = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "web-sg"
  }
}
