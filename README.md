# deploy-environments

[deploy-environments](https://gitlab.com/steve.szombath/deploy-environments) project
is GitLab Pages project that generates static web content and hosts in on its [pages](https://steve.szombath.gitlab.io/deploy-environments/).
The content serves as **training material**.

If you require something please leave an [Issue](https://gitlab.com/steve.szombath/deploy-environments/issues).

# Technical Details

Static web content generator used: [mkdocs](http://mkdocs.org).
Publisher and Content Provider: GitLab [Pages](https://about.gitlab.com/product/pages/).