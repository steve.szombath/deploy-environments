# IaC next level

* IaC under version control
    * code and config
    * review
    * test / plan
       * static verification
       * dry run
    * IaC is a product on its own
    * generate docs (that is often a requirement)
    
```
    *   37244c4  Merge branch 'enable_autoscaling' into 'master' <istvan.szombath@evosoft.com>
    |\
    | * ec533d4  feat: quality gates added <istvan.szombath@evosoft.com>
    | * 5fc4832  fix: Could not get lock problem <***.***@evosoft.com>
    | * 98db395  Disable autoscaling <***.***@evosoft.com>
    | * ab97de6  Enable autoscaling <***.***@evosoft.com>
    |/
    *   405912e  Merge branch '3-set-up-test-environment' into 'master' <***.***@evosoft.com>
```

* IaC: Continuous
    * test
    * integrate
    * deploy

```mermaid
graph LR;
  plan-->apply;
  apply-->destroy;
```

* separate config from code
* layered environments (modularity)
    * networking layer
    * container orchestration layer
    * microservice layer

```
$ ls
CHANGELOG.md
CONTRIBUTING.md
json2tf.py
LICENSE
qa-eu-central-1.tfvars
README.md
s3_setup.sh
scaffold.tf

$ cat scaffold.tf | grep module
module "reference_architecture"
```

