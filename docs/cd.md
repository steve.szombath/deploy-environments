# Continuous Delivery / Deployment

## Environments

```mermaid
graph LR;
  A[int]
  B[pre-prod]
  C[prod]
  A--deploy-->B;
  B--deploy-->C;
  A--feature tests-->A;
  B--e2e tests-->B;
  C--smoke tests-->C;
```

* local
    * limited feasibility in microservice / IoT world
* dev - sandbox for developers
    * unit testing
    * few components only
    * team ownership
    * use components from other environments?
* **int** - integration
    * DoD
    * low on resources
        * NFR?
    * 9-5/5
    * shared horse
* testing
    * is it necessary?
    * performance / load tests
    * security penetration tests
    * disaster recovery exercise
    * high costs... unless...
    * **one button full deploy**
* **pre-prod** -  mirror of production environment
    * networking!
    * resources?
    * dogfooding / early access :-)
    * exposed only to friendly users
    * user demo / user acceptance
    * no support
    * no confidential data
* **prod** - exposed to all users
    * and to the Internet
        * security
    * confidential / sensitive data
    * 24/7 availability (99%)
    * plus support
* on-premise
    * cloud / IoT paranoia
    * legal reasons
    * extra costs 
    * feature rollout?
    * extra features?
    * [Azure On-Premise](https://azure.microsoft.com/en-us/resources/videos/microsoft-azure-stack-azure-services-on-premises/)
    * [AWS DevCloud](https://aws.amazon.com/govcloud-us/)
![GovCloud](https://d1.awsstatic.com/WWPS/AWSGovCloud(US)_Logo_shape.4362c7a76a7ad8cff30d7978c5a42ef8762283a2.png) 

## Workflow

**Test, deploy, repeat**.

* test
    * unit / component tests
    * inter service tests
        * integration
        * feature
        * e2e 
* deploy
    * downtime?
    * schedule with testing?
* repeat
