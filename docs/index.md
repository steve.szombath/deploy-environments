# Deploy Environments 

István Szombath

Test Architect @ Evosoft, MindSphere **IoT Platform** (Siemens)

[doc source](https://gitlab.com/steve.szombath/deploy-environments)

## DevOps Cycle

![DevOps](https://docs.microsoft.com/en-us/azure/devops/learn/_img/devops-cycle.png)

* Continuously **deploy** **why**?
    * focus on working product
    * small changes
    * rapid feedback
    * *reduce product risk*  
* Continuously **deploy** **what** / **when** / **where** / **who**?
    * single microservice?
        * into existing environment(s)
    * whole product?
        * multiple microservices
        * into new environment
    * (environment?)
        * infrastructure is a product on its own
        * reusable
