# Challenges

* zero downtime deploy
* even for IaC change
* one environment (to rule them all)
* e2e testing

## Deployment Strategies

* Big Bang
* Rolling Deploy
* [Canary Release](https://martinfowler.com/bliki/CanaryRelease.html)
* [Blue Green Deployment](https://martinfowler.com/bliki/BlueGreenDeployment.html)
![AWS_ECS_BG](https://3ovyg21t17l11k49tk1oma21-wpengine.netdna-ssl.com/wp-content/uploads/2017/10/Implementing-Blue-Green-with-ECS.png)
* **Rainbow Deploy**
![Rainbow DevOps](https://pbs.twimg.com/media/CEQOsL9XIAAezy_.png)

## [Feature Toogles](https://martinfowler.com/articles/feature-toggles.html)

![Feature Toogles](https://martinfowler.com/articles/feature-toggles/chart-6.png)

* release toogles
* experiment toogles
* ops toogles
* **permission toogles**
