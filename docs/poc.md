## Proof of Concept

### Terraform

* create a VPC
* create 1 public and 1 private subnet
    * associate routing tables
* create 1 EC2 in public subnet
  * add IGW
  * add security group
  * install webserver 
* test webserver

```
sudo apt-get -y update
sudo apt-get -y install nginx
sudo service nginx start
```

### Ansible

* govern EC2
