# [Terraform](https://www.terraform.io/) 

Infrastructure as Code to provision and manage **any cloud**, infrastructure, or service.

## What is Terraform

DSL to build, change and version infrastructure.

* IaC
* version control
* declarative
* resource dependency graph
* plan (dry run)
* reproducible environments
* shared [modules](https://github.com/terraform-aws-modules)
* multiple [providers](https://www.terraform.io/docs/providers/index.html)

![resource graph](https://www.terraform.io/assets/images/docs/graph-example-8a4f085e.png)

![providers](https://www.terraform.io/assets/images/terraform-overview/infrastructure-providers@4x-91e3d4c9.png)

## How it works

* declare desired state
* generates execution plan (DAG)
* executes it

Changes:

* determines change
* incremental execution 

## How to install

Download, unzip, put it into the `$PATH`.

```
curl -O -L https://releases.hashicorp.com/terraform/0.12.13/terraform_0.12.13_linux_amd64.zip
unzip terraform_0.12.13_linux_amd64.zip
./terraform -help
```

## Configuration and providers

Terraform describes infrastructure using *configuration files* (`*.tf`).
Create a configuration file  e.g. `main.tf` and add:

```
provider "aws" {
  region     = "eu-central-1"
}
```

## Resources

Defines resource that exists in the infrastructure, both physical or logical.

```
resource "aws_instance" "my_aws_instance" {
  ami           = "ami-0f3a43fbf2d3899f7"
  subnet_id     = "subnet-09e4f88f541fe0936"
  instance_type = "t2.nano"
}
```

Please note that ami id and subnet id is subject to change.

## Credentials

## Terraform workflow

Check *aws instances* on web console during exercise:

* `terraform init`
* `terraform plan`
* `terraform apply`
* `terraform destroy`

## tfstate and changes

`Apply complete! Resources: 1 added, 0 changed, 0 destroyed.`

Check `terraform.tfstate` file content.

* add to resource `tags = { "Name" = "my-instance" }`
* change `instance_type = "t2.micro"`
* change `ami = "ami-010fae13a16763bb4"`

## Immutable infrastructure

## Further features

* remote tfstate e.g. AWS S3
* modules, variables, inputs
* provisioner
* terraform version managers
* [Terragrunt](https://github.com/gruntwork-io/terragrunt)