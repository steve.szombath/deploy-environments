# Infrastructure as Code

History:

* snowflake environments
    * arbitrary changes through lifecycle
    * non-documented
* describe assets (bill of material)
    * e.g. microservice (semantic) versions
    * some kind of CMDB
        * docs / wiki / spreadsheets
        * DB / ~~professional tooling~~
        * master architecture diagram
    ![Master Architecture Diagram](img/master.jpg)
* how to assemble environments
    * docs
    * often requirement
* **domain specific language (DSL)**
    * e.g. terraform
* **unambiguous** interpretation
