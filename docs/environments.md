# Environment branches

[GitFlow](https://nvie.com/posts/a-successful-git-branching-model/)
![GitFlow](https://nvie.com/img/git-model@2x.png)

* link branches to environments
    * new node on named branch initiates deploy
    * preferably merge node
    * environment variables
    * [Gitlab Flow](https://about.gitlab.com/blog/2014/09/29/gitlab-flow/)
    * branches can be pointers to previous states of master, e.g. [OneFlow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
    
![GitLab Flow](https://about.gitlab.com/images/git_flow/environment_branches.png)
![OneFlow](https://www.endoflineblog.com/img/oneflow/develop-hotfix-branch-merge-final.png)

* IaC with product repo or separate repo?
    * build once
    * CI / CD for one product / microservice may separated into two repos
 
```yaml
variables:
  DEPLOY_TARGET:        "DEV"
  VERSION:              "1.47.3-5-c0e4d120"
```
